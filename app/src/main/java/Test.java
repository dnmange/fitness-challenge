import android.util.Log;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Dell Pc on 29-04-2018.
 */

public class Test {
    public static void main (String[] args) throws java.lang.Exception
    {
        String date = "2018-02-15 09:59 PM";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd K:mm a");
        try{
            Date dateObj = dateFormat.parse(date);
            date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(dateObj);
        }catch (Exception e){
            //Log.d(TAG, e.getMessage());
        }
        //dateFormat.getTime();
        Timestamp timeStamp = Timestamp.valueOf(date);
        System.out.println(timeStamp.getTime());
    }
}
