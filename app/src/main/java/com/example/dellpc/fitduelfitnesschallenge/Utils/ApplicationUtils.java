package com.example.dellpc.fitduelfitnesschallenge.Utils;

import android.content.Context;
import android.widget.Toast;

import com.example.dellpc.fitduelfitnesschallenge.LoginActivity;

/**
 * Created by Dell Pc on 24-04-2018.
 */

public class ApplicationUtils {

    private static ApplicationUtils instance;

    public static ApplicationUtils getInstance(){
        if(instance!=null)
            return instance;
        return new ApplicationUtils();
    }

    public void displayToast(Context context, String message){
        Toast.makeText(context, message,
                Toast.LENGTH_SHORT).show();
    }
}
