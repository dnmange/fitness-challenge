package com.example.dellpc.fitduelfitnesschallenge;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dellpc.fitduelfitnesschallenge.model.ChallengeDetails;
import com.example.dellpc.fitduelfitnesschallenge.service.PedometerService;
import com.example.dellpc.fitduelfitnesschallenge.statAndUpcomingChallenge.ChallengeFragment;
import com.example.dellpc.fitduelfitnesschallenge.user_profile.UserProfileFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import java.io.IOException;

public class ProfileStatsChallengesActivity extends AppCompatActivity implements ChallengeFragment.OnFragmentInteractionListener,UserProfileFragment.OnFragmentInterfaceListener{

    private static final int CHALLENGE_DETAILS = 2;
    private static final int PICK_IMAGE_REQUEST = 7;
    private static final String TAG = "ProfileStatsChallengesActivity";
    private int action=-1;
    private String userId;
    private Uri filePath;
    private Intent pedometer;
    private String challengeId;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private SharedPreferences sharedPreferencesChallenges;
    private ChallengeDetails challengeDetails;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_challenges:
                    loadUpcomingChallenge();
                    return true;
                case R.id.navigation_profile:
                    loadUserProfile();
                    return true;
                case R.id.navigation_stats:
                    loadUserStats();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_stat_challenges);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        if(savedInstanceState==null){
            loadUpcomingChallenge();
        }
        sharedPreferencesChallenges = getSharedPreferences("challengeDataService",0);
        getActiveChallenge();
    }


    private void getActiveChallenge() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference reference = database.getReference("active_challenge");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                challengeId = (String)dataSnapshot.getValue();
                getChallengeDetails();
                reference.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    protected void getChallengeDetails(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference reference = database.getReference("daily_challenges/"+challengeId);

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                challengeDetails = dataSnapshot.getValue(ChallengeDetails.class);
                challengeDetails.setId(dataSnapshot.getKey());
                startServiceForChallenge();
                reference.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    protected void startServiceForChallenge() {

        pedometer = new Intent(this, PedometerService.class);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference reference = database.getReference("challenge_result/"+challengeId+"/"+userId+"/steps");

        if(reference!=null){
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if(dataSnapshot.getValue()!=null){
                        if(!isServiceRunning(PedometerService.class)){
                            long numSteps = (long)dataSnapshot.getValue();
                            sharedPreferencesChallenges = getSharedPreferences("challengeDataService",0);
                            pedometer.putExtra("userId",userId);
                            pedometer.putExtra("challengeId",challengeDetails.getId());
                            pedometer.putExtra("startTime",challengeDetails.getStartDate());
                            pedometer.putExtra("endTime",challengeDetails.getEndDate());
                            pedometer.putExtra("numSteps",numSteps);

                            startService(pedometer);
                        }
                    }
                    reference.removeEventListener(this);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }

    private void loadFragment(Fragment fragment, Bundle args ){
        fragment.setArguments(args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container_profile_stats_challenge, fragment);
        transaction.commit();
    }

    protected void loadUpcomingChallenge(){
        Bundle args = new Bundle();
        args.putString("identifier",getString(R.string.upcoming_challenges));
        loadFragment(new ChallengeFragment(),args);
    }

    protected void loadUserProfile(){
        Bundle args = new Bundle();
        String userName = getIntent().getStringExtra("fullName");
        userId = getIntent().getStringExtra("id");
        args.putString("userId",userId);
        args.putString("fullName",userName);
        loadFragment(new UserProfileFragment(),args);
    }

    protected void loadUserStats(){
        Bundle args = new Bundle();
        args.putString("identifier",getString(R.string.stats));
        args.putString("userId",userId);
        loadFragment(new ChallengeFragment(),args);
    }

    @Override
    public void challengeItemClicked(String identifier, ChallengeDetails challengeDetails) {
        Intent showChallenge = new Intent(this,ChallengeDetailedActivity.class);
        String details = new Gson().toJson(challengeDetails);
        showChallenge.putExtra("challengeDetails", details);
        showChallenge.putExtra("identifier",identifier);
        showChallenge.putExtra("userId",userId);
        startActivityForResult(showChallenge,CHALLENGE_DETAILS);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            ImageView profilePicture = findViewById(R.id.profile_photo);
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                profilePicture.setImageBitmap(bitmap);
                uploadImage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            return;
        }

        if (requestCode != CHALLENGE_DETAILS) {
            return;
        }
        switch (resultCode){
            case RESULT_OK:
                if(data.getExtras()!=null){
                    this.action = (int)data.getExtras().get("action");

                }
                break;
            case RESULT_CANCELED:
                break;
        }
    }

    protected void uploadImage() {
        if (filePath != null) {

            StorageReference reference = storageReference.child("images/"+userId);
            reference.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @SuppressLint("LongLogTag")
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Log.d(TAG,"image uploaded to firebase");
                            updateUserURI();
                        }
                    });
        }
    }

    protected void updateUserURI() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setPhotoUri(filePath)
                .build();
        user.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Log.d(TAG,"User URI updated");
                }
            }
        });
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        changeListBasedOnAction();
    }

    protected void changeListBasedOnAction(){

        this.action = -1;
    }

    @Override
    public void onBackPressed(){

    }


    @Override
    public void onChooseImage() {
        Intent imageChooser;
        if (Build.VERSION.SDK_INT < 19){
            imageChooser = new Intent();
            imageChooser.setAction(Intent.ACTION_GET_CONTENT);
            imageChooser.setType("image/*");
            startActivityForResult(imageChooser, PICK_IMAGE_REQUEST);
        } else {
            imageChooser = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            imageChooser.addCategory(Intent.CATEGORY_OPENABLE);
            imageChooser.setType("image/*");
            startActivityForResult(imageChooser, PICK_IMAGE_REQUEST);
        }
    }
}
