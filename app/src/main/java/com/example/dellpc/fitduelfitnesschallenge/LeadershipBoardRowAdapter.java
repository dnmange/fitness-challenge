package com.example.dellpc.fitduelfitnesschallenge;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.dellpc.fitduelfitnesschallenge.model.LeadershipBoardRow;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by nakulNarwaria on 11-05-2018.
 */

public class LeadershipBoardRowAdapter extends ArrayAdapter<LeadershipBoardRow> {

    Context context;

    public LeadershipBoardRowAdapter(Context context, int resourceId,
                                     List<LeadershipBoardRow> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    private class ViewHolder {
        TextView rank;
        TextView name;
        TextView steps;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        LeadershipBoardRow rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.leadership_board_row, null);
            holder = new ViewHolder();
            holder.rank = (TextView) convertView.findViewById(R.id.rank);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.steps = (TextView) convertView.findViewById(R.id.steps);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.rank.setText(rowItem.getRank());
        holder.name.setText(rowItem.getName());
        holder.steps.setText(rowItem.getSteps());

        return convertView;
    }
}
