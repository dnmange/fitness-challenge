package com.example.dellpc.fitduelfitnesschallenge;

import android.annotation.TargetApi;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NavUtils;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.dellpc.fitduelfitnesschallenge.challengeRecyclerView.ChallengeAdapter;
import com.example.dellpc.fitduelfitnesschallenge.model.ChallengeDetails;
import com.example.dellpc.fitduelfitnesschallenge.service.MyFirebaseMessagingService;
import com.example.dellpc.fitduelfitnesschallenge.service.PedometerService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.w3c.dom.Text;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.example.dellpc.fitduelfitnesschallenge.constants.ApplicationConstants.PATTERN_12_HR;
import static com.example.dellpc.fitduelfitnesschallenge.constants.ApplicationConstants.PATTERN_24_HR;

public class ChallengeDetailedActivity extends AppCompatActivity {

    private static final String TAG = "ChallengeDetailed";
    private ChallengeDetails challengeDetails;
    private String identifier;
    private boolean challengeEnded=false;
    private TextView challengeName;
    private TextView fee;
    private TextView totalPrize;
    private TextView maxEntries;
    private TextView maxFootSteps;
    private TextView minFootSteps;
    private TextView startDate;
    private TextView endDate;
    private TextView myStepsField;
    private TableRow mySteps;
    private Button submitBtn;

    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private String userId;
    private boolean isRegistered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_detailed);

        challengeDetails = new Gson().fromJson(getIntent().getStringExtra("challengeDetails"), ChallengeDetails.class);
        if(FirebaseAuth.getInstance()== null|| FirebaseAuth.getInstance().getCurrentUser() == null){
            Intent loginIntent = new Intent(this, LoginActivity.class);
            NavUtils.navigateUpTo(this,loginIntent);
        }
        userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        challengeName = findViewById(R.id.challenge_name);
        fee = findViewById(R.id.challenge_fee);
        totalPrize = findViewById(R.id.challenge_total_prize);
        maxEntries = findViewById(R.id.challenge_max_entries);
        mySteps = findViewById(R.id.my_steps);
        minFootSteps = findViewById(R.id.challenge_min_steps);
        maxFootSteps = findViewById(R.id.challenge_max_steps);
        startDate = findViewById(R.id.challenge_start_date);
        endDate = findViewById(R.id.challenge_end_date);
        submitBtn = findViewById(R.id.submit_button);
        myStepsField = findViewById(R.id.challenge_my_steps);
        mySteps.setVisibility(View.INVISIBLE);

        checkUserRegisteredForChallenge();
        setFields();
    }

    protected void setFields(){
        challengeName.setText(challengeDetails.getName()+"");
        fee.setText(challengeDetails.getFee());
        totalPrize.setText(challengeDetails.getTotalPrize());
        maxFootSteps.setText(challengeDetails.getMaxFootSteps());
        minFootSteps.setText(challengeDetails.getMinFootSteps());
        startDate.setText(challengeDetails.getStartDate());
        endDate.setText(challengeDetails.getEndDate());
        maxEntries.setText(challengeDetails.getMaxEntries());
        try {
            String endDate = challengeDetails.getEndDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
            Date date =  sdf.parse(endDate);
            if(date.getTime()< Calendar.getInstance().getTime().getTime())
                challengeEnded=true;

        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    protected void checkUserRegisteredForChallenge(){
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("challenge_users"+"/"+challengeDetails.getId());
        if(myRef!=null) {
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for(DataSnapshot challengeIdSnapshot : dataSnapshot.getChildren()){
                        if(challengeIdSnapshot.getKey().equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))
                        {
                            isRegistered = true;
                            break;
                        }
                    }
                    if(isRegistered){
                        submitBtn.setText(R.string.leaderBoard);
                        setStepsUI();
                    }else{
                        submitBtn.setText(R.string.register);
                        if(challengeEnded)
                            submitBtn.setEnabled(false);

                    }
                    submitBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(!isRegistered){
                                registerUserInFireBase();
                            }else{
                                Intent leadershipBoard = new Intent(getApplicationContext(), LeadershipBoard.class);
                                leadershipBoard.putExtra("Challenge Id",challengeDetails.getId());
                                leadershipBoard.putExtra("User Id",getIntent().getStringExtra("userId"));
                                startActivity(leadershipBoard);
                            }
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d(TAG,databaseError.getMessage());
                }
            });
        }
    }

    protected void setStepsUI(){
        mySteps.setVisibility(View.VISIBLE);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("challenge_result/"+challengeDetails.getId()+"/"+userId+"/steps");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                myStepsField.setText(dataSnapshot.getValue()+"");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    protected void saveUserInfoInChallengeResult(int numSteps){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("challenge_result/"+challengeDetails.getId());
        DatabaseReference childRef = databaseReference.child(userId);
        HashMap<String, Object> map = new HashMap<>();
        map.put("steps",numSteps);
        map.put("name", FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
        childRef.setValue(map);
    }

    protected void registerUserInFireBase(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference challengeUserReference = database.getReference("challenge_users/"+challengeDetails.getId());
        DatabaseReference userChallengeReference = database.getReference("user_challenges/"+userId);

        DatabaseReference childUser = challengeUserReference.child(userId);
        childUser.setValue(true);

        DatabaseReference childChallenge = userChallengeReference.child(challengeDetails.getId());
        childChallenge.setValue(true);

        saveUserInfoInChallengeResult(0);
        sendNotification(getString(R.string.you_are_registered_for_the_challenge),challengeDetails.getName());

        startPedometerService();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void startPedometerServiceWithForeground(){
        Intent pedometer = new Intent(this, PedometerService.class);
        long startDate = convertStringIntoTimeStamp(challengeDetails.getStartDate());
        long endDate = convertStringIntoTimeStamp(challengeDetails.getEndDate());

        pedometer.putExtra("userId",userId);
        pedometer.putExtra("challengeId",challengeDetails.getId());
        pedometer.putExtra("startTime",challengeDetails.getStartDate());
        pedometer.putExtra("endTime",challengeDetails.getEndDate());

        startForegroundService(pedometer);

    }

    protected void startPedometerService(){
        Intent pedometer = new Intent(this, PedometerService.class);
        long startDate = convertStringIntoTimeStamp(challengeDetails.getStartDate());
        long endDate = convertStringIntoTimeStamp(challengeDetails.getEndDate());

        pedometer.putExtra("userId",userId);
        pedometer.putExtra("challengeId",challengeDetails.getId());
        pedometer.putExtra("startTime",challengeDetails.getStartDate());
        pedometer.putExtra("endTime",challengeDetails.getEndDate());
        //pedometer.putExtra("numSteps",0);

        startService(pedometer);
    }

    @TargetApi(Build.VERSION_CODES.O)
    protected void startPedometerForegroundService(Intent pedometerIntent){
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, pedometerIntent, 0);

        Notification notification =
                new Notification.Builder(this, "Pedometer service")
                        .setContentTitle(challengeDetails.getName())
                        .setContentText(challengeDetails.getMinFootSteps()+"")
                        .setContentIntent(pendingIntent)
                        .build();


    }

    protected long convertStringIntoTimeStamp(String date) {
        DateFormat dateFormat = new SimpleDateFormat(PATTERN_12_HR);
        try{
            Date dateObj = dateFormat.parse(date);
            date = new SimpleDateFormat(PATTERN_24_HR).format(dateObj);
        }catch (Exception e){
            Log.d(TAG, e.getMessage());
        }
        Timestamp timeStamp = Timestamp.valueOf(date);
        return timeStamp.getTime();
    }

    public void sendNotification(String title,String messageBody) {
        Intent showChallenge = new Intent(this,ChallengeDetailedActivity.class);
        showChallenge.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        String details = new Gson().toJson(challengeDetails);
        showChallenge.putExtra("challengeDetails", details);
        showChallenge.putExtra("identifier",identifier);
        showChallenge.putExtra("userId",userId);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, showChallenge,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_challenge)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 , notificationBuilder.build());
    }

}
