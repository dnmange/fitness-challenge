package com.example.dellpc.fitduelfitnesschallenge.model;

/**
 * Created by nakulNarwaria on 11-05-2018.
 */

public class LeadershipBoardRow {
    private String rank;
    private String name;
    private String steps;

    public LeadershipBoardRow(String rank,String name,String steps){
        this.setRank(rank);
        this.setName(name);
        this.setSteps(steps);
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }
}
