package com.example.dellpc.fitduelfitnesschallenge.challengeRecyclerView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dellpc.fitduelfitnesschallenge.R;
import com.example.dellpc.fitduelfitnesschallenge.model.ChallengeDetails;

import org.w3c.dom.Text;

/**
 * Created by Dell Pc on 01-04-2018.
 */

public class ChallengeHolder extends RecyclerView.ViewHolder{
    private TextView challengeName;
    private TextView challengeFee;
    private TextView challengeMaxSteps;
    private TextView challengeTotalPrize;
    private Context context;

    public ChallengeHolder(View view, ViewGroup parent, Context context){
        super(view);
        this.context = context;
        challengeName=  itemView.findViewById(R.id.challenge_name);
        challengeFee =  itemView.findViewById(R.id.challenge_fee);
        challengeMaxSteps =  itemView.findViewById(R.id.challenge_max_steps);
        challengeTotalPrize =  itemView.findViewById(R.id.challenge_total_prize);
    }


    public void bind(ChallengeDetails challengeDetails){
        challengeName.setText("Challenge Name:"+challengeDetails.getName());
        challengeFee.setText("Fee:"+challengeDetails.getFee()+""+context.getString(R.string.usd));
        challengeMaxSteps.setText("Max Foot Steps:"+challengeDetails.getMaxFootSteps());
        challengeTotalPrize.setText("Total Prize Fee:"+challengeDetails.getTotalPrize());
    }

}
