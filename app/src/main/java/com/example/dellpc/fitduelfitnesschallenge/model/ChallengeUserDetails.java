package com.example.dellpc.fitduelfitnesschallenge.model;

import com.example.dellpc.fitduelfitnesschallenge.R;

/**
 * Created by nakulNarwaria on 29-04-2018.
 */

public class ChallengeUserDetails implements Comparable<ChallengeUserDetails>{
    private int steps;
    private String userName;


    public int compareTo(ChallengeUserDetails m)
    {
        return m.steps - this.steps;
    }

    public ChallengeUserDetails(int steps, String userName)
    {
        this.steps = steps;
        this.userName = userName;
    }

    public int getSteps() { return steps; }
    public String getUserName()   {
        if(userName == null)
        {
            return "Anonymous";
        }
        else
           return userName;
    }



}
