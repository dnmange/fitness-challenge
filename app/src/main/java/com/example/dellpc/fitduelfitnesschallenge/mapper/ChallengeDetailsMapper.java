package com.example.dellpc.fitduelfitnesschallenge.mapper;

import com.example.dellpc.fitduelfitnesschallenge.challengeRecyclerView.ChallengeAdapter;
import com.example.dellpc.fitduelfitnesschallenge.model.ChallengeDetails;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dell Pc on 26-04-2018.
 */

public class ChallengeDetailsMapper {
    private ChallengeDetails challengeDetails;
    private static ChallengeDetailsMapper challengeDetailsMapper;

    public static ChallengeDetailsMapper getInstance(){
        if(challengeDetailsMapper!=null)
            return challengeDetailsMapper;
        return new ChallengeDetailsMapper();
    }

    public ChallengeDetails getChallengeDetails(){
        return challengeDetails;
    }

    public void setDetails(JSONObject obj) throws JSONException{
        challengeDetails.setEndDate(obj.has("endDate") ? obj.getString("endDate") : "");
        challengeDetails.setEndDate(obj.has("startDate") ? obj.getString("startDate") : "");
        challengeDetails.setEndDate(obj.has("fee") ? obj.getString("fee") : "");
        challengeDetails.setEndDate(obj.has("maxEntries") ? obj.getString("maxEntries") : "");
        challengeDetails.setEndDate(obj.has("maxFootSteps") ? obj.getString("maxFootSteps") : "");
        challengeDetails.setEndDate(obj.has("minFootSteps") ? obj.getString("minFootSteps") : "");
        challengeDetails.setEndDate(obj.has("name") ? obj.getString("name") : "");
        challengeDetails.setEndDate(obj.has("totalPrize") ? obj.getString("totalPrize") : "");
    }
}
