package com.example.dellpc.fitduelfitnesschallenge.user_profile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dellpc.fitduelfitnesschallenge.LoginActivity;
import com.example.dellpc.fitduelfitnesschallenge.R;
import com.example.dellpc.fitduelfitnesschallenge.service.PedometerService;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.ProfilePictureView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.SENSOR_SERVICE;
import static com.facebook.FacebookSdk.getApplicationContext;

public class UserProfileFragment extends Fragment implements View.OnClickListener {

    private static final int PICK_IMAGE_REQUEST = 7;
    private static final String TAG = "UserProfileFragment";
    //private OnFragmentInteractionListener mListener;

    private String userId;
    private String userName;
    private TextView userEmail;
    private TextView nameView;
    //private ProfilePictureView profilePicture;
    private EditText subject;
    private EditText message;
    private Button submit;
    private Button logOutButton;
    private ImageView profilePicture;
    private FirebaseUser user;


    public UserProfileFragment() {

    }

    public interface OnFragmentInterfaceListener{
        public void onChooseImage();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();
        userName = getArguments() != null ? getArguments().getString("fullName"):null;
        View profile_display = inflater.inflate(R.layout.fragment_user_profile, container, false);
        profilePicture = (ImageView)profile_display.findViewById(R.id.profile_photo);
        nameView = profile_display.findViewById(R.id.name);
        subject = profile_display.findViewById(R.id.subjectET);
        message = profile_display.findViewById(R.id.messageET);
        submit= profile_display.findViewById(R.id.submitButton);
        userEmail = profile_display.findViewById(R.id.userEmail);


        submit.setOnClickListener(this);
        nameView.setText(user.getDisplayName());
        userEmail.setText(user.getEmail());


        logOutButton = profile_display.findViewById(R.id.logOutButton);

        logOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pedometer = new Intent(getActivity(), PedometerService.class);
                FirebaseAuth.getInstance().signOut();
                LoginManager.getInstance().logOut();
                getActivity().stopService(pedometer);
                Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
                NavUtils.navigateUpTo(getActivity(),loginIntent);
            }
        });

        profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserProfileFragment.OnFragmentInterfaceListener fragmentInterfaceListener= (UserProfileFragment.OnFragmentInterfaceListener)getActivity();
                fragmentInterfaceListener.onChooseImage();
                profilePicture.setBackground(null);
            }
        });

        try{
            setProfilePicture();
        }catch (Exception e){
            Log.d(TAG,e.getMessage());
        }
        if(userId.isEmpty())
            Toast.makeText(getApplicationContext(),getString(R.string.no_user_name),Toast.LENGTH_SHORT).show();

        return profile_display;
    }

    protected void setProfilePicture() throws IOException {
        if(user.getPhotoUrl()!=null){
            Bitmap bitmap;
            if((user.getPhotoUrl()+"").contains("facebook")){
                setFacebookProfilePicture();
            }else {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), user.getPhotoUrl());
                profilePicture.setImageBitmap(bitmap);
                profilePicture.setBackground(null);

            }

        }
    }

    protected void setFacebookProfilePicture(){
        try{
            String fullPictureUrl = user.getPhotoUrl()+""+"?type=large";
            Picasso.get().load(fullPictureUrl).into(profilePicture);
            profilePicture.setBackground(null);

        }catch (Exception e){
            Log.d(TAG,e.getMessage());
        }
    }

    @Override
    public void onClick(View source) {
        if(source == submit){
            Intent email = new Intent(Intent.ACTION_SEND);
            email.setType("message/rfc822");
            email.putExtra(Intent.EXTRA_EMAIL  , new String[]{"nakulnarwaria17@gmail.com"});
            email.putExtra(Intent.EXTRA_SUBJECT, subject.getText());
            email.putExtra(Intent.EXTRA_TEXT   , message.getText());
            try {
                startActivity(Intent.createChooser(email, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getApplicationContext(), getString(R.string.no_email_client), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
