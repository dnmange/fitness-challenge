package com.example.dellpc.fitduelfitnesschallenge.statAndUpcomingChallenge;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dellpc.fitduelfitnesschallenge.R;
import com.example.dellpc.fitduelfitnesschallenge.challengeRecyclerView.ChallengeAdapter;
import com.example.dellpc.fitduelfitnesschallenge.model.ChallengeDetails;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.Challenge;

public class ChallengeFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private RecyclerView challengeRecyclerView;
    private ChallengeAdapter challengeAdapter;
    private String identifier;
    private List<ChallengeDetails> challenges;

    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private String userId;
    private long challengeCount;
    private FirebaseUser user;

    public ChallengeFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_challenge, container, false);
        identifier = getArguments() != null ? getArguments().getString("identifier"):null;
        user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();
        if(identifier.equals(getString(R.string.upcoming_challenges))){
            getChallengeDataFromFireBase();
        }else if(identifier.equals(getString(R.string.stats))){
            getUserRelatedChallengesFromFireBase();
        }

        challengeRecyclerView = (RecyclerView) view
                .findViewById(R.id.challenge_recycler_view);
        challengeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    protected void getChallengeDataFromFireBase(){
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("daily_challenges");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                challenges = new ArrayList<>();
                for(DataSnapshot challengeIdSnapshot : dataSnapshot.getChildren()){
                    ChallengeDetails challengeDetails = null;
                    challengeDetails = challengeIdSnapshot.getValue(ChallengeDetails.class);
                    challengeDetails.setId((String)challengeIdSnapshot.getKey());
                    challenges.add(challengeDetails);

                }

                Collections.sort(challenges);
                challengeAdapter = new ChallengeAdapter(challenges,getActivity(),identifier,challengeRecyclerView);
                challengeRecyclerView.setAdapter(challengeAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    protected void getUserRelatedChallengesFromFireBase(){
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("user_challenges"+"/"+userId);
        if(myRef!=null) {
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    challenges = new ArrayList<>();
                    challengeCount = dataSnapshot.getChildrenCount();
                    for(DataSnapshot challengeIdSnapshot : dataSnapshot.getChildren()){
                        setChallengeForUserInUI((String)challengeIdSnapshot.getKey());
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    protected void setChallengeForUserInUI(String challengeId) {
        myRef = database.getReference("daily_challenges/"+challengeId);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ChallengeDetails challengeDetails = dataSnapshot.getValue(ChallengeDetails.class);
                if(dataSnapshot.getValue()==null)
                {
                    Collections.sort(challenges);
                    challengeAdapter = new ChallengeAdapter(challenges,getActivity(),identifier,challengeRecyclerView);
                    challengeRecyclerView.setAdapter(challengeAdapter);
                    return;
                }

                challengeDetails.setId((String)dataSnapshot.getKey());

                challenges.add(challengeDetails);
                if(--challengeCount==0){
                    Collections.sort(challenges);
                    challengeAdapter = new ChallengeAdapter(challenges,getActivity(),identifier,challengeRecyclerView);
                    challengeRecyclerView.setAdapter(challengeAdapter);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void challengeItemClicked(String identifier,ChallengeDetails challengeDetails);
    }
}
