package com.example.dellpc.fitduelfitnesschallenge.pedometer;

/**
 * Created by Dell Pc on 18-04-2018.
 * AUTHOR THIRD PARTY
 */

public interface StepListener {

    public void step(long timeNs);

}
