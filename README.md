#Fitduel Fitness Challenge
---
(Developers: Darshan Mange and Nakul Narwaria)

FitDuel is an application that presents users with daily fitness challenges that users can enroll in and compete with each other. FitDuel uses firebase for user analytics and cloud messaging, phone's acceleriometer for counting footsteps via service, facebook graph API for login and fetching user data. It has many modules namely leaderboard, challenge registration, sign up, facebook or email login and user stats. Future scope for this application is to use Stripe API for money transactions so that money can be pooled from user registration fee and awarded as prize to the winner of the challenge so more and more users will be interested to join the app. Also, acceleriometer will be replaced by fitbit API.



## Scenarios Tested
---
1. User login via facebook and profile picture along with other data is fetched and displayed in profile section.
2. User sign up functionality which registeres user to the firebase database.
3. Profile photo along with other data user provides on sign up is saved in firebase or not.
4. List of challeneges that are posted daily.
5. List of challeneges user is enrolled in, displayed in stats section.
6. Challenges are in proper order of time posted or not.
7. Leadership board associated with each challenege which can be accessed from the challenge details page ONLY if user is registered in the challenge. 
8. Whether user can register in current challenges or not.
9. Pedometer service is working in the challenge run time or not. 
10. Leadership board is updated with different users working on chanllenge or not.
11. Service resumes when activity is recreated or not.
12. Notification is received when user register for a challenge or not.
13. Notifications for new challenges are received or not.
14. Support feature is working or not.  
15. User registers for the challenge, challenge starts and steps are calculated.
16. User can change profile picture by clickin on the profile photo on profile section in the app.  

## Issues
---
1) As going forward fitbit api will be used instead of Pedometer service, and it was not feasible to maintain the state of service for various users in the app, we decided to calculate the steps when user is logged in and if he signs out, service cannot be started again for that particular user on that particular challenge.  
2) As our app is not live, for you to test facebook login, we need to send you the request to add you as Developer/Tester, than only you will be able to logged in via facebook. 

## Note
---
To check whether Pedometer service is working or not, shake phone in up down movement and check whether value in MySteps field in Challenge Details Activity is increasing.

## ThirdParty Code
---
We just wrote the service of Pedometer, actual calculation of steps were done using 3rd party 
Picasso was used to download image from facebook
